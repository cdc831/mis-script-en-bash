-----------------------------------------------------------------------------------------------------------------------------------------------------------
# TASK: CopyProduct
# 3scale help
env

function copiarATenant(){
   # Obtenemos el ID producto
   export ID=$(3scale service show https://${TOKEN_ORIGEN}@${TENANT_ORIGEN} ${PRODUCT_SYSTEM_NAME} -k | tail -n -1 | awk '{print $1}')
   
   # Se procede a copiar todo al tenant destino
   3scale -k --verbose product copy -s https://${TOKEN_ORIGEN}@${TENANT_ORIGEN} -d https://${TOKEN_DESTINO}@${TENANT_DESTINO} $ID
}

function borrarTodo(){
	# Buscamos si existe el servicio en el destino, en caso correcto se obtiene el ID
	export ID=$(3scale service show https://${TOKEN_DESTINO}@${TENANT_DESTINO} ${PRODUCT_SYSTEM_NAME} -k | tail -n -1 | awk '{print $1}')

	# Obtenemos el listado de backends y lo almacenamos
	curl -v -k "https://${TENANT_DESTINO}/admin/api/services/$ID/backend_usages.json?access_token=${TOKEN_DESTINO}" | jq -e -r '.[].backend_usage.backend_id' > lbb.txt

	## Borrado del servicio o producto.
	3scale -k --verbose service delete https://${TOKEN_DESTINO}@${TENANT_DESTINO} $ID
    	echo "Producto: ( ${PRODUCT_SYSTEM_NAME} ) del tenant destino borrado correctamente!."
    	sleep 5
	
	# Recorremos el listado de backend asociado y lo eliminamos
	for backend_id in `cat lbb.txt`; do
		curl -v -k -X DELETE "https://${TENANT_DESTINO}/admin/api/backend_apis/$backend_id.json" -d "access_token=${TOKEN_DESTINO}"
		sleep 1
	done
}

# Buscamos si existe el servicio en el destino, en caso correcto se obtiene el ID
export ID_DESTINO=$(3scale service show https://${TOKEN_DESTINO}@${TENANT_DESTINO} ${PRODUCT_SYSTEM_NAME} -k | tail -n -1 | awk '{print $1}')

# Chequeamos que la variable no sea nula.
if [ -z "$ID_DESTINO" ]
then
	echo "No se encontró el producto: ( ${PRODUCT_SYSTEM_NAME} ) en el destino, se procede el copiado completo." 
	sleep 5m
	copiarATenant
else
	# El servicio existe. Se procede a borrar el servicios con sus backends correspondientes.
	## Borramos El producto y los backend asociados.
	borrarTodo
	
	## Esperamos unos momentos para darle tiempo al borrado en el tenant destino.	
	echo 'Aguarde mientras se realiza el borrado. La tarea demorará unos minutos.'
	sleep 5m

	## Se procede al copiado completo
	copiarATenant
fi

-----------------------------------------------------------------------------------------------------------------------------------------------------------
# TASK: Rename
# Obtenemos el ID producto

# Buscamos si existe el servicio en el destino, en caso correcto se obtiene el ID
export ID=$(3scale service show https://${TOKEN_DESTINO}@${TENANT_DESTINO} ${PRODUCT_SYSTEM_NAME} -k | tail -n -1 | awk '{print $1}')

# Obtenemos el listado de backends y lo almacenamos
curl -v  -k "https://${TENANT_DESTINO}/admin/api/services/$ID/backend_usages.json?access_token=${TOKEN_DESTINO}" | jq -e -r '.[].backend_usage.backend_id' > lb.txt

for backend_id in `cat lb.txt`; do
    # Obtenemos los nombres ej: listar-cuotaprestamos
    export SYSTEMNAME=$(curl -v  -k "https://${TENANT_DESTINO}/admin/api/backend_apis/$backend_id?access_token=${TOKEN_DESTINO}" | jq -e -r '.backend_api.system_name')

    # Completamos la ruta completa
    export URL_COMPLETA="http://$SYSTEMNAME.$PRODUCT_SYSTEM_NAME-$TENANT_DEV/api/$PRODUCT_SYSTEM_NAME"

    # Reemplazamos la url
    curl -v -k -X PUT "https://${TENANT_DESTINO}/admin/api/backend_apis/$backend_id.json" -d "access_token=${TOKEN_DESTINO}&private_endpoint=$URL_COMPLETA"
done

-----------------------------------------------------------------------------------------------------------------------------------------------------------
# TASK: Rename V2
# Obtenemos el ID producto

# Buscamos si existe el servicio en el destino, en caso correcto se obtiene el ID
export ID=$(3scale service show https://${TOKEN_DESTINO}@${TENANT_DESTINO} ${PRODUCT_SYSTEM_NAME} -k | tail -n -1 | awk '{print $1}')

# Obtenemos el listado de backends y lo almacenamos
curl -v  -k "https://${TENANT_DESTINO}/admin/api/services/$ID/backend_usages.json?access_token=${TOKEN_DESTINO}" | jq -e -r '.[].backend_usage.backend_id' > lb.txt

for backend_id in `cat lb.txt`; do
    # Obtenemos los nombres ej: listar-cuotaprestamos
    export ROUTES=$(curl -v  -k "https://${TENANT_DESTINO}/admin/api/backend_apis/$backend_id.json?access_token=${TOKEN_DESTINO}" | jq -e -r '.backend_api.private_endpoint | sub("dev"; "test")')
	echo -e "LOG::$ROUTES"
    # Reemplazamos la url
    curl -v -k -X PUT "https://${TENANT_DESTINO}/admin/api/backend_apis/$backend_id.json" -d "access_token=${TOKEN_DESTINO}&private_endpoint=$ROUTES"
done
-----------------------------------------------------------------------------------------------------------------------------------------------------------

http://listar-cobrosprestamos.prestamos-dev.svc.cluster.local:8080/api/prestamos/0220004345/cobros


http://listar-acuerdos.cuentas-dev.svc.cluster.local:8080/api/prestamos/0220004345/cobros

http://lp/0220004345/cobros
mis
/clientes/123/productos
/prouctos/123/propietarios


TENANT_DEV	dev.svc.cluster.local:8080/

TENANT_ORIGEN   sandbox-admin.apps.ocp04.ciudad.bco

TENANT_DESTINO  dev-admin.apps.ocp04.ciudad.bco

TOKEN_ORIGEN    6ab667111fcca6f3fb3a0ec337cf1877a967ea76deb27e37dc792532f9200926

TOKEN_DESTINO   64ee78a26e07d5a07db2d3b01da3516038b73c4c62e11a813e146e167e942d71
-----------------------------------------------------------------------------------------------------------------------------------------------------------

Las mejoras a incorporar son las siguientes:

1 - URI de contexto en bash para el mapeo de servicios 3scale -> Cluster
2 - Contratos con las URIs correctas (/api/<nombre_producto>)
3 - Modificación de nombres de backends en 3scale (debieran corresponderse con el nombre del servicio del cluster) 
	Ejemplo: System Name de backend en 3scale (nombre de interno para el objeto backend): listar-movimientos 
		 Nombre de deployment en openshift: listar-movimientos)
4 - Configuración de policy para cross domain en todos los productos. Esto habilita el uso desde el portal de cliente
5 - Evaluar posibilidad de implementar un merge entre productos de distintos ambientes, o en su defecto borrarlo y crearlo desde 0  ********** <----------
6 - Remover manifiesto de rutas de repo de gitops
7 - Extender la promoción hacia el cluster OCP03 (tenants staging y prod.)

-----------------------------------------------------------------------------------------------------------------------------------------------------------
CONSULTAR TODOS LOS SERVICIO
curl -v  -X GET "https://sandbox-admin.apps.ocp04.ciudad.bco/admin/api/services.xml?access_token=6ab667111fcca6f3fb3a0ec337cf1877a967ea76deb27e37dc792532f9200926&page=1&per_page=500"

CONSULTA SERVICIO POR ID
curl -v  -X GET "https://sandbox-admin.apps.ocp04.ciudad.bco/admin/api/services/17.xml?access_token=6ab667111fcca6f3fb3a0ec337cf1877a967ea76deb27e37dc792532f9200926"

ELIMINA SERVICIO POR ID
curl -v  -X DELETE "https://dev-admin.apps.ocp04.ciudad.bco/admin/api/services/25.xml" -d 'access_token=64ee78a26e07d5a07db2d3b01da3516038b73c4c62e11a813e146e167e942d71'

ELIMINA BACKEND POR ID
curl -v  -X DELETE "https://dev-admin.apps.ocp04.ciudad.bco/admin/api/backend_apis/90.json" -d 'access_token=64ee78a26e07d5a07db2d3b01da3516038b73c4c62e11a813e146e167e942d71'

-----------------------------------------------------------------------------------------------------------------------------------------------------------
CONSULTA HTTP
STATUS=$(curl -I https://www.google.com.ar 2> /dev/null | head -n 1 | awk -F" " '{print $2}')

if [ $STATUS == 200 ]
then
   echo "Exito!"
else
   echo "Mal :|"
fi

-----------------------------------------------------------------------------------------------------------------------------------------------------------
PROGRESS BAR
echo 'Aguarde mientras se realiza el borrado. La tarea demorará unos minutos.'

BAR='[##################################################################################################'
POR='0 %'

for i in {1..100}; do
	echo -ne "\r${BAR:0:$i} $i %" 
	sleep .1
done

echo ']'

-----------------------------------------------------------------------------------------------------------------------------------------------------------

curl -v -k -X DELETE "https://dev-admin.apps.ocp04.ciudad.bco/admin/api/services/28.xml" -d 'access_token=64ee78a26e07d5a07db2d3b01da3516038b73c4c62e11a813e146e167e942d71'


curl -v  -k "https://${TENANT_ORIGEN}/admin/api/backend_apis/$backend_id.json?access_token=${TOKEN_ORIGEN}" | jq -e -r '.backend_api.private_endpoint | sub("dev"; "test")' >> out.txt



curl -v  -k "https://dev-admin.apps.ocp04.ciudad.bco/admin/api/backend_apis/$backend_id.json?access_token=${TENANT_DESTINO}" | jq -e -r '.backend_api.private_endpoint | sub("dev"; "test")'