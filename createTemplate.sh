#!/bin/bash
# Autor: Cristian Cantero
# Creacion de proyectos gitops-argo


# Variables

input_opcion=""
input_project="cuentas"
input_app="listar-acuerdos"
input_gitops_base="git@ssh.dev.azure.com:v3/BCBA-CIC/Openshift/openshift-gitops"
input_gitops_repo="git@ssh.dev.azure.com:v3/BCBA-CIC/Cuentas/listar-acuerdos-gitops"
input_rama_dev="main"
input_rama_test="main"


echo -e "
1) Crear App Nueva
2) Crear proyecto nuevo"
read -p "Elija una opcion: " input_opcion

if (($input_opcion == 1)); then

# Escribimos el yaml de la App Dev - Test
read -p "Cual es el nombre de la App nueva: " input_app
read -p "Cual es el el gitops: " input_gitops_repo

echo -e "apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: $input_app-dev
  namespace: openshift-gitops
spec:
  destination:
    namespace: $input_project-dev
    server: https://kubernetes.default.svc
  project: $input_project
  source:
    path: overlay/dev
    repoURL: $input_gitops_repo
    targetRevision: $input_rama_dev
  syncPolicy:
    automated:
      selfHeal: false
    syncOptions:
    - Validate=false
    - ApplyOutOfSyncOnly=true" >> build/$input_project/dev/apps/$input_app.yaml


echo -e "apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: $input_app-test
  namespace: openshift-gitops
spec:
  destination:
    namespace: $input_project-test
    server: https://kubernetes.default.svc
  project: $input_project
  source:
    path: overlay/test
    repoURL: $input_gitops_repo
    targetRevision: $input_rama_test
  syncPolicy:
    automated:
      selfHeal: false
    syncOptions:
    - Validate=false
    - ApplyOutOfSyncOnly=true" >> build/$input_project/test/apps/$input_app.yaml
fi


if (($input_opcion == 2)); then

#if [ -d "$demoApp" ]
#then
#   echo "El directorio ${DIRECTORIO} existe"
#else
#   mkdir -m 777 build/
#fi

#if [ -f $FICHERO ]
#then
#   echo "El fichero $FICHERO existe"
#else
#   echo "El fichero $FICHERO no existe"
#fi

# Creamos los directorios bases
mkdir -m 777 build/ &> /dev/null
mkdir -m 777 build/$input_project &> /dev/null
## Dev
mkdir -m 777 build/$input_project/dev &> /dev/null
mkdir -m 777 build/$input_project/dev/apps &> /dev/null
mkdir -m 777 build/$input_project/dev/project &> /dev/null
## Test
mkdir -m 777 build/$input_project/test > /dev/null 2>&1
mkdir -m 777 build/$input_project/test/apps > /dev/null 2>&1
mkdir -m 777 build/$input_project/test/project > /dev/null 2>&1


# Escribimos el archivo Yamls raiz
echo -e "apiVersion: v1
kind: List
metadata: {}
items:
- apiVersion: argoproj.io/v1alpha1
  kind: AppProject
  metadata:
    finalizers:
    - resources-finalizer.argocd.argoproj.io
    name: $input_project
  spec:
    clusterResourceWhitelist:
    - group: '*'
      kind: Namespace
    - group: argoproj.io
      kind: Application
    - group: argoproj.io
      kind: AppProject
    - group: argoproj.io
      kind: ApplicationSet
    - group: '*'
      kind: Secret
    - group: '*'
      kind: PersistentVolumeClaim
    - group: '*'
      kind: Pipeline
    - group: '*'
      kind: ServiceAccount
    - group: '*'
      kind: LimitRange
    - group: '*'
      kind: ResourceQuota

    destinations:
    - namespace: openshift-gitops
      server: https://kubernetes.default.svc
    - namespace: $input_project-dev
      server: https://kubernetes.default.svc
    - namespace: $input_project-test
      server: https://kubernetes.default.svc
    namespaceResourceBlacklist:
    - group: ''
      kind: NetworkPolicy
    sourceRepos:
    - '*'
- apiVersion: argoproj.io/v1alpha1
  kind: ApplicationSet
  metadata:
    name: $input_project-set
  spec:
    generators:
    - list:
        elements:
        - env: dev
          project: $input_project
        - env: test
          project: $input_project
    template:
      metadata:
        finalizers:
        - resources-finalizer.argocd.argoproj.io
        name: '{{project}}-{{env}}'
        namespace: openshift-gitops
      spec:
        destination:
          namespace: '{{project}}-{{env}}'
          server: https://kubernetes.default.svc
        project: '{{project}}'
        source:
          directory:
            recurse: true
          path: ocp04/proyectos/{{project}}/{{env}}
          repoURL: $input_gitops_base
          targetRevision: main
        syncPolicy:
          automated:
            selfHeal: true
          syncOptions:
          - Validate=false
          - CreateNamespace=true
          - ApplyOutOfSyncOnly=true" >> build/$input_project-project.yaml

# Escribimos el namespace Dev - Test
echo -e "apiVersion: v1
items:
- apiVersion: v1
  kind: Namespace
  metadata:
    name: $input_project-dev
kind: List
metadata: {}" >> build/$input_project/dev/project/namespace.yaml

echo -e "apiVersion: v1
items:
- apiVersion: v1
  kind: Namespace
  metadata:
    name: $input_project-test
kind: List
metadata: {}" >> build/$input_project/test/project/namespace.yaml

# Escribimos el RoleBinding Dev - Test

echo -e "kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: ocp-Dev02
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: admin
subjects:
- apiGroup: rbac.authorization.k8s.io
  kind: Group
  name: ocp02-Dev
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: azure-agent-rb
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: azure-agent-role
subjects:
- kind: ServiceAccount
  name: azure-agent
  namespace: devops-system" >> build/$input_project/dev/project/rolebinding.yaml

echo -e "kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: ocp-Dev02
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: admin
subjects:
- apiGroup: rbac.authorization.k8s.io
  kind: Group
  name: ocp02-Dev
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: azure-agent-rb
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: azure-agent-role
subjects:
- kind: ServiceAccount
  name: azure-agent
  namespace: devops-system" >> build/$input_project/test/project/rolebinding.yaml

# Escribimos el yaml de la App Dev - Test

echo -e "apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: $input_app-dev
  namespace: openshift-gitops
spec:
  destination:
    namespace: $input_project-dev
    server: https://kubernetes.default.svc
  project: $input_project
  source:
    path: overlay/dev
    repoURL: $input_gitops_repo
    targetRevision: $input_rama_dev
  syncPolicy:
    automated:
      selfHeal: false
    syncOptions:
    - Validate=false
    - ApplyOutOfSyncOnly=true" >> build/$input_project/dev/apps/$input_app.yaml


echo -e "apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: $input_app-test
  namespace: openshift-gitops
spec:
  destination:
    namespace: $input_project-test
    server: https://kubernetes.default.svc
  project: $input_project
  source:
    path: overlay/test
    repoURL: $input_gitops_repo
    targetRevision: $input_rama_test
  syncPolicy:
    automated:
      selfHeal: false
    syncOptions:
    - Validate=false
    - ApplyOutOfSyncOnly=true" >> build/$input_project/test/apps/$input_app.yaml
fi
