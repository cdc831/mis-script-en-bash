# Nos paramos en la carpeta correspondiente donde se aloja el cli OC
cd /azp

# Obtenemos todos los servicios del producto.
./oc get svc/ -n ${NAMESPACE}-dev | awk '! /NAME/ {print$1}'  > servicios.txt

# Recorremos los servicios para obtener las etiquetas.
for obDesc in `cat servicios.txt`; do

done


# TASK: Rename v1.1

# Nos paramos en la carpeta correspondiente donde se aloja el cli OC
cd /azp

# Buscamos si existe el servicio en el destino, en caso correcto se obtiene el ID
export ID=$(3scale service show https://${TOKEN_DESTINO}@${TENANT_DESTINO} ${PRODUCT_SYSTEM_NAME} -k | tail -n -1 | awk '{print $1}')

# Obtenemos el listado de backends y lo almacenamos
curl -v  -k "https://${TENANT_DESTINO}/admin/api/services/$ID/backend_usages.json?access_token=${TOKEN_DESTINO}" | jq -e -r '.[].backend_usage.backend_id' > lb.txt

for backend_id in `cat lb.txt`; do
    # Obtenemos los nombres ej: listar-cuotaprestamos
    export SYSTEMNAME=$(curl -v  -k "https://${TENANT_DESTINO}/admin/api/backend_apis/$backend_id?access_token=${TOKEN_DESTINO}" | jq -e -r '.backend_api.system_name')
    
    export NAME=$(./oc describe svc/$SYSTEMNAME -n ${PRODUCT_SYSTEM_NAME}-dev | grep -w "Name" | awk '{print$2}')
    echo "LOG:: NAME: $NAME"
    export NAMESPACE=$(./oc describe svc/$SYSTEMNAME -n ${PRODUCT_SYSTEM_NAME}-dev | grep -w "Namespace" | awk '{print$2}')
    echo "LOG:: NAMESPACE: $NAMESPACE"
    export CONTEXT=$(./oc describe svc/$SYSTEMNAME -n ${PRODUCT_SYSTEM_NAME}-dev | grep Annotations | awk '{print$3}')
    if [ -z "$CONTEXT" ]
    then
        echo -e "No se encontro la anotacion Context en el service."
        exit 1
    else
        echo "LOG:: CONTEXT: $CONTEXT"
    fi

    # Completamos la ruta completa
    export URL_COMPLETA="http://$NAME.$NAMESPACE.svc.cluster.local:8080$CONTEXT"
    
    echo "LOG:: URL_COMPLETA: $URL_COMPLETA"

    # Reemplazamos la url
    curl -v -k -X PUT "https://${TENANT_DESTINO}/admin/api/backend_apis/$backend_id.json" -d "access_token=${TOKEN_DESTINO}&private_endpoint=$URL_COMPLETA"
done
